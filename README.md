"GROUP FINAL MOBILE PROGRAMMING PROJECT"

Our group's final project is a comprehensive online platform designed to provide a seamless and user-friendly shopping experience. We have created a dynamic application that caters to a wide range of requirements assigned in each categories.

Our e-commerce project aims to deliver a visually appealing and intuitive interface that enhances the user's browsing and shopping journey. With a focus on user experience, we have implemented advanced search and filtering functionalities, allowing customers to easily find the products they are looking for.

In addition to the mobile app functionalities, our project includes interactive product pages that display detailed information, high-resolution images, easy authenticating, and related products. As a group, we have collaborated closely to ensure the project's success, dividing tasks and responsibilities to leverage each team member's strengths. We have conducted thorough testing and implemented rigorous quality control measures to ensure a bug-free and reliable website.

Our e-commerce project showcases our skills in mobile application development, user experience design, and project management. Through this project, we have gained valuable experience in creating a comprehensive online application platform that meets the needs and expectations of both client and users.


Bonghanoy
Bual
Catampatan
Cultura
Ratunil

January 2024