import React, { useState } from 'react';
import { Text, View, TextInput, StyleSheet, ActivityIndicator, ImageBackground } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import ThemeButton from '../components/ThemeButton';
import { storeUser, validateForm } from './authSlice'; 

const backgroundImage = require('../assets/images/bg1.png');

export default function Register() {
    const dispatch = useDispatch();
    const { user, errors } = useSelector((state) => state.auth);

    const [formData, setFormData] = useState({
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
    });

    const [passwordStrength, setPasswordStrength] = useState('');
    const [successMessage, setSuccessMessage] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const checkPasswordStrength = (password) => {
        if (password.length < 6) {
            return 'Weak';
        } else if (password.length < 10) {
            return 'Medium';
        } else {
            return 'Strong';
        }
    };

    const handlePasswordChange = (value) => {
        setFormData({ ...formData, password: value });
        const strength = checkPasswordStrength(value);
        setPasswordStrength(strength);
    };

    const handleRegister = () => {
        setIsLoading(true);
        dispatch(storeUser(formData));
        dispatch(validateForm());
        setFormData({ ...formData, usernameError: '', emailError: '', passwordError: '', confirmPasswordError: '' });
    
        let isValid = true;
    
        if (!formData.username) {
            setFormData((prevData) => ({
                ...prevData,
                usernameError: 'Username is required',
            }));
            isValid = false;
        }
    
        if (!formData.email) {
            setFormData((prevData) => ({
                ...prevData,
                emailError: 'Email is required',
            }));
            isValid = false;
        } else if (!isValidEmail(formData.email)) {
            setFormData((prevData) => ({
                ...prevData,
                emailError: 'Invalid email format',
            }));
            isValid = false;
        }
    
        if (formData.password.length < 6) {
            setFormData((prevData) => ({
                ...prevData,
                passwordError: 'Password must be at least 6 characters',
            }));
            isValid = false;
        }
    
        if (formData.password !== formData.confirmPassword) {
            setFormData((prevData) => ({
                ...prevData,
                confirmPasswordError: 'Passwords do not match',
            }));
            isValid = false;
        }
    
        if (isValid) {
            setTimeout(() => {
                setIsLoading(false);
                setSuccessMessage('Registration successful');
            }, 2000);
        } else {
            setIsLoading(false);
        }
    };
    
    const isValidEmail = (email) => {
        const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailPattern.test(email);
    };

    return (
        <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
            <View style={styles.container}>
                <Text style={styles.title}></Text>

        <View style={styles.container}>
            <Text style={styles.title}>Register Here</Text>
            <TextInput
                style={styles.input}
                placeholder="Username"
                value={formData.username}
                onChangeText={(value) => setFormData({ ...formData, username: value })}
            />
            <Text style={styles.errorText}>{errors.usernameError}</Text>

            <TextInput
                style={styles.input}
                placeholder="Email"
                value={formData.email}
                onChangeText={(value) => setFormData({ ...formData, email: value })}
            />
            <Text style={styles.errorText}>{errors.emailError}</Text>

            <TextInput
                style={styles.input}
                placeholder="Password"
                secureTextEntry={true}
                value={formData.password}
                onChangeText={handlePasswordChange}
            />
            <Text style={{ ...styles.errorText, color: getPasswordStrengthColor(passwordStrength) }}>
                {passwordStrength}
            </Text>
            <Text style={styles.errorText}>{errors.passwordError}</Text>

            <TextInput
                style={styles.input}
                placeholder="Confirm Password"
                secureTextEntry={true}
                value={formData.confirmPassword}
                onChangeText={(value) => setFormData({ ...formData, confirmPassword: value })}
            />
            <Text style={styles.errorText}>{errors.confirmPasswordError}</Text>

            {isLoading ? (
                <ActivityIndicator size="large" color="blue" style={styles.loadingIndicator} />
            ) : (
                <>
                    {successMessage && <Text style={styles.successText}>{successMessage}</Text>}
                    <ThemeButton title="Submit" onPress={handleRegister} />
                </>
            )}
        </View>
        </View>
        </ImageBackground>
    );
}

const getPasswordStrengthColor = (strength) => {
    switch (strength) {
        case 'Weak':
            return 'red';
        case 'Medium':
            return 'orange';
        case 'Strong':
            return 'green';
        default:
            return 'black';
    };
};

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
    },
    title: {
        fontSize: 24,
        marginBottom: 20,
    },
    input: {
        width: 300,
        height: 40,
        borderColor: 'gray',
        backgroundColor: '#F2F2F2',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 10,
        marginBottom: 10,
    },
    errorText: {
        color: 'red',
        fontSize: 14,
    },
    successText: {
        color: 'green',
        fontSize: 14,
    },
    loadingIndicator: {
        marginTop: 20,
    },
});
