import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const CartScreen = ({ route, navigation }) => {
  const { cartItems } = route.params;
  const [cart, setCart] = useState(cartItems || []);

  const renderCartItem = ({ item }) => (
    <View style={styles.cartItem}>
      <View style={styles.column}>
        <Text style={styles.columnHeader}>Product</Text>
        <Text style={styles.itemName}>{item.name}</Text>
      </View>
      <View style={styles.column}>
        <Text style={styles.columnHeader}>Item Price</Text>
        <Text style={styles.itemPrice}>{item.price}</Text>
      </View>
      <View style={styles.column}>
        <Text style={styles.columnHeader}>Actions</Text>
        <View style={styles.actions}>
          <TouchableOpacity onPress={() => removeItem(item.id)}>
            <Icon name="trash" size={20} color="#e74c3c" />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );

  const removeItem = (itemId) => {
    const updatedCart = cart.filter((item) => item.id !== itemId);
    setCart(updatedCart);
  };

  const calculateTotalPrice = () => {
    return cart.reduce((total, item) => {
      const itemPrice = parseFloat(item.price.slice(1));
      return total + (isNaN(itemPrice) ? 0 : itemPrice);
    }, 0);
  };

  const checkout = () => {
    // payment method options
    Alert.alert(
      'Choose Payment Method',
      'Select a payment method for your order:',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Cash on Delivery',
          onPress: () => {
            Alert.alert(
              'Success',
              'Order placed successfully!',
              [
                {
                  text: 'OK',
                  onPress: () => {
                    setCart([]); 
                  },
                },
              ],
              { cancelable: false }
            );
          },
        },
        {
          text: 'Online Payment',
          onPress: () => {
            Alert.alert(
              'Online Payment',
              'Online payment is not available at the moment. Please try again later.'
            );
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Cart</Text>
      <FlatList
        data={cart}
        keyExtractor={(item) => item.id.toString()}
        renderItem={renderCartItem}
      />
      <View style={styles.totalContainer}>
        <Text style={styles.totalText}>Total: ${calculateTotalPrice()}</Text>
        <TouchableOpacity style={styles.checkoutButton} onPress={checkout}>
          <Text style={styles.checkoutButtonText}>Checkout</Text>
        </TouchableOpacity>
      </View>

      {/* Footer Section */}
      <View style={styles.footer}>
        <TouchableOpacity onPress={() => navigation.navigate('DashBoard')}>
          <Icon name="home" size={30} color="#3498db" />
          <Text style={styles.footerText}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('UserProfile')}>
          <Icon name="user" size={30} color="#3498db" />
          <Text style={styles.footerText}>Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Cart', { cartItems: [] })}>
          <Icon name="shopping-cart" size={30} color="#3498db" />
          <Text style={styles.footerText}>Cart</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 1,
    backgroundColor: "#2E294E", 
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: "#c0392b", 
  },
  cartItem: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 10,
    backgroundColor: "#f7dc6f", 
    padding: 10,
    borderRadius: 5,
  },
  column: {
    flex: 1,
    marginLeft: 10,
  },
  columnHeader: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
    color: "#27ae60", 
  },
  itemName: {
    fontSize: 16,
  },
  itemPrice: {
    fontSize: 16,
    marginLeft: 10,
    color: "#2c3e50", 
  },
  actions: {
    marginLeft: 10,
  },
  totalContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    paddingVertical: 10,
  },
  totalText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: "#27ae60", 
  },
  checkoutButton: {
    backgroundColor: '#3498db',
    padding: 10,
    borderRadius: 5,
  },
  checkoutButtonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    paddingVertical: 10,
    backgroundColor: "#6A0572", 
  },
  footerText: {
    textAlign: 'center',
    color: '#00A6A6', 
    marginTop: 5,
  },
  productFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
export default CartScreen;