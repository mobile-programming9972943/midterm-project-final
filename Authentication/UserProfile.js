import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, Text, Alert } from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';
import { storeUser, validateForm } from '../Authentication/authSlice';
import Icon from 'react-native-vector-icons/FontAwesome';

const UserProfile = ({ navigation }) => {
  const dispatch = useDispatch();
  const reduxUserInfo = useSelector((state) => state.auth.user);

  const [userInfo, setUserInfo] = useState({
    username: 'username',
    location: 'Cagayan de Oro City',
    contactNumber: '123-456-7890',
    birthday: '00/00/0000',
    newPassword: '',
  });

  useEffect(() => {
    setUserInfo(reduxUserInfo);
    console.log('User information updated:', reduxUserInfo);
  }, [reduxUserInfo]);

  const handleSave = () => {
    dispatch(storeUser(userInfo));
    console.log('User information saved:', userInfo);
  };

  const handleChangePassword = () => {
    console.log('Password changed:', userInfo.newPassword);
  };

  const handleLogout = () => {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'Logout',
          onPress: () => {
            // Handle logout logic here
            console.log('User logged out');
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.cardImageContainer}>
        <Image source={require('../assets/images/1144760.png')} style={styles.cardImage} />
      </View>
      <View>
        <Text style={styles.username}>{userInfo.username}</Text>
        <TextInput
          label="Location"
          value={userInfo.location}
          onChangeText={(text) => setUserInfo({ ...userInfo, location: text })}
          style={styles.input}
        />
        <TextInput
          label="Contact Number"
          value={userInfo.contactNumber}
          onChangeText={(text) => setUserInfo({ ...userInfo, contactNumber: text })}
          style={styles.input}
        />
        <TextInput
          label="Birthday"
          value={userInfo.birthday}
          onChangeText={(text) => setUserInfo({ ...userInfo, birthday: text })}
          style={styles.input}
        />
        <TextInput
          label="New Password"
          secureTextEntry
          value={userInfo.newPassword}
          onChangeText={(text) => setUserInfo({ ...userInfo, newPassword: text })}
          style={styles.input}
        />
      </View>
      <View style={styles.actions}>
        <Button mode="contained" onPress={handleSave} style={styles.saveButton}>
          Save
        </Button>
        <Button mode="contained" onPress={handleChangePassword} style={styles.saveButton}>
          Change Password
        </Button>
      </View>

      {/* Footer Section */}
      <View style={styles.footer}>
        <TouchableOpacity onPress={() => navigation.navigate('DashBoard')}>
          <Icon name="home" size={30} color="#3498db" />
          <Text style={styles.footerText}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('UserProfile')}>
          <Icon name="user" size={30} color="#3498db" />
          <Text style={styles.footerText}>Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Cart', { cartItems: [] })}>
          <Icon name="shopping-cart" size={30} color="#3498db" />
          <Text style={styles.footerText}>Cart</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    padding: 1,
  },
  cardImageContainer: {
    alignItems: 'center',
  },
  cardImage: {
    width: 150,
    height: 150,
  },
  username: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 10,
    color: 'black',
  },
  input: {
    marginBottom: 10,
    backgroundColor: '#805C87',
    color: 'black'
  },
  actions: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
  },
  saveButton: {
    flex: 1,
    marginHorizontal: 5,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    paddingVertical: 10,
    backgroundColor: '#6A0572',
  },
  footerText: {
    textAlign: 'center',
    color: '#00A6A6',
    marginTop: 5,
  },
});

export default UserProfile;
