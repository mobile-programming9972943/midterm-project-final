import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const ProductDetails = ({ route }) => {
  const { productId, allProducts } = route.params;

  const getProductDetails = (id) => {
    if (allProducts) {
      const selectedProduct = allProducts.find((product) => product.id === id);
      return selectedProduct || {}; 
    }
    return {}; 
  };

  const selectedProduct = getProductDetails(productId);

  const buyNow = () => {
    console.log(`Buying product "${selectedProduct.name}".`);
  };

  return (
    <View style={styles.container}>
      <Image source={selectedProduct.image} style={styles.productImage} />
      <Text style={styles.productName}>{selectedProduct.name}</Text>
      <Text style={styles.productDescription}>{selectedProduct.description}</Text>
      <Text style={styles.productPrice}>Price: {selectedProduct.price}</Text>

      <TouchableOpacity style={styles.buyNowButton} onPress={buyNow}>
        <Icon name="credit-card" size={20} color="#fff" />
        <Text style={styles.buttonText}>Buy Now</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center', 
    backgroundColor: "#F39C12", 
  },
  productImage: {
    width: 300, 
    height: 300, 
    borderRadius: 10,
    resizeMode: 'cover',
  },
  productName: {
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 10,
    color: "#C0392B", // Dark Red
  },
  productDescription: {
    fontSize: 16,
    color: '#333',
    marginVertical: 10,
  },
  productPrice: {
    fontSize: 18,
    color: '#3498db',
    fontWeight: 'bold',
  },
  buyNowButton: {
    backgroundColor: '#27ae60',
    padding: 10,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
  },
  buttonText: {
    color: '#fff',
    marginLeft: 10,
  },
});

export default ProductDetails;