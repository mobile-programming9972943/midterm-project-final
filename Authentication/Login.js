import React, { useState, useEffect } from 'react';
import { Text, View, TextInput, StyleSheet, ImageBackground, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import ThemeButton from '../components/ThemeButton';
import LoadingScreen from '../components/LoadingScreen';

import { storeUser } from './authSlice';

const backgroundImage = require('../assets/images/bg1.png');

export default function Login({ navigation }) {
    const { user } = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState({
        username: '',
        password: '',
    });

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false);
        }, 2000); // Simulated 2-second loading delay
    }, []);

    const handleLogin = () => {
        if (!data.username || !data.password) {
            Alert.alert('Please enter your username & password');
            return;
        }

        const isUserRegistered = user && user.username === data.username;

        if (isUserRegistered) {
            if (user.password === data.password) {
                dispatch(storeUser(data));
                navigation.navigate('DashBoard');
            } else {
                Alert.alert('The password you entered is incorrect. Please try again');
            }
        } else {
            Alert.alert(
                'The username you entered isn’t connected to an account.',
                'Would you like to register now?',
                [
                    {
                        text: 'No',
                        onPress: () => console.log('User decided not to register'),
                        style: 'cancel',
                    },
                    {
                        text: 'Yes',
                        onPress: () => navigation.navigate('Register'),
                    },
                ]
            );
        }
    };

    if (isLoading) {
        return <LoadingScreen />;
    }

    return (
        <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
            <View style={styles.container}>
                <Text style={styles.title}>Login</Text>

                {/* Username Input Field */}
                <TextInput
                    style={styles.input}
                    placeholder="Username"
                    value={data.username}
                    onChangeText={(value) => {
                        setData({
                            ...data,
                            username: value,
                        });
                    }}
                />

                {/* Password Input Field */}
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    value={data.password}
                    secureTextEntry={true}
                    onChangeText={(value) => {
                        setData({
                            ...data,
                            password: value,
                        });
                    }}
                />

                {/* Login Button with Console Log */}
                <ThemeButton title="Login" onPress={handleLogin} />

                <Text style={styles.signupText}>
                    Don't have an account?{' '}
                    <Text
                        style={styles.signupLink}
                        onPress={() => {
                            navigation.navigate('Register');
                        }}
                    >
                        Sign up
                    </Text>
                </Text>

                <Text style={styles.forgotPasswordLink} onPress={() => {
                    navigation.navigate('ForgotPassword');
                }}>
                    Forgot Password?
                </Text>
            </View>

            {/* Footer */}
            <View style={styles.footer}>
                <Text style={styles.footerText}>STEP UP FOOTWEAR</Text>
            </View>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
    },
    title: {
        fontSize: 24,
        marginBottom: 20,
        color: 'black',
    },
    input: {
        width: '100%',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 10,
        marginBottom: 10,
        backgroundColor: '#F2F2F2',
    },
    signupText: {
        marginTop: 10,
        fontSize: 16,
        color: 'black',
    },
    signupLink: {
        color: 'blue',
        textDecorationLine: 'underline',
    },
    forgotPasswordLink: {
        color: 'blue',
        textDecorationLine: 'underline',
        marginTop: 10,
    },
    footer: {
        backgroundColor: '#F2F2F2',
        padding: 10,
        alignItems: 'center',
    },
    footerText: {
        color: 'black',
    },
});
