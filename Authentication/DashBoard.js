import React, { useState } from 'react';
import { StyleSheet, View, Text, Image, ScrollView, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const featuredProducts = [
  {
    id: 1,
    name: 'Air Max Thea',
    price: '$50',
    image: require("../assets/images/product20.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 2,
    name: 'Air Zoom 39',
    price: '$70',
    image: require("../assets/images/product1.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 3,
    name: 'Daybreak Type',
    price: '$90',
    image: require("../assets/images/product3.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 7,
    name: 'Waffle One',
    price: '$90',
    image: require("../assets/images/product4.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 8,
    name: 'Air Presto',
    price: '$90',
    image: require("../assets/images/product5.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 9,
    name: 'Hyperfuse Elite',
    price: '$90',
    image: require("../assets/images/product8.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 10,
    name: 'Air Furyosa',
    price: '$90',
    image: require("../assets/images/product9.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  // Add more featured product data
];

const newArrivals = [
  {
    id: 4,
    name: 'Blazer Mid',
    price: '$60',
    image: require("../assets/images/product10.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 5,
    name: 'Air Max 95',
    price: '$80',
    image: require("../assets/images/product12.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 6,
    name: 'ZX 8000',
    price: '$100',
    image: require("../assets/images/product14.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 11,
    name: 'Forum Low',
    price: '$100',
    image: require("../assets/images/product17.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 12,
    name: 'Yung-1',
    price: '$100',
    image: require("../assets/images/product18.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  {
    id: 13,
    name: 'Deerupt',
    price: '$100',
    image: require("../assets/images/product19.jpg"),
    description: 'Size: 38, 39, 40, 41, 42',
  },
  // Add more new arrival product data
];


const HomeScreen = ({ navigation }) => {
  const [cartItems, setCartItems] = useState([]);

  const renderProductItem = (item, index) => (
    <TouchableOpacity
      key={`product_${item.id}_${index}`}
      style={styles.productCard}
      onPress={() => navigation.navigate('ProductDetails', { productId: item.id, allProducts: [...featuredProducts, ...newArrivals] })}
    >
      <Image source={item.image} style={styles.productImage} />
      <Text style={styles.productName}>{item.name}</Text>
      <View style={styles.productFooter}>
        <Text style={styles.productPrice}>{item.price}</Text>
        <TouchableOpacity onPress={() => addToCart(item)}>
          <Icon name="shopping-cart" size={20} color="#3498db" />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );

  const addToCart = (item) => {
    setCartItems((prevItems) => [...prevItems, item]);
  };

  const navigateToProfile = () => {
    navigation.navigate('UserProfile');
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Step Up</Text>
      </View>
      <ScrollView style={styles.body}>
        <Text style={styles.sectionTitle}>Featured Products</Text>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={styles.productList}
        >
          {featuredProducts.map(renderProductItem)}
        </ScrollView>
        
        {}
        <Text style={styles.sectionTitle}>New Arrivals</Text>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          style={styles.productList}
        >
          {newArrivals.map(renderProductItem)}
        </ScrollView>
      </ScrollView>
      <View style={styles.footer}>
        <TouchableOpacity onPress={() => navigation.navigate('DashBoard')}>
          <Icon name="home" size={30} color="#3498db" />
          <Text style={styles.footerText}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('UserProfile')}>
          <Icon name="user" size={30} color="#3498db" />
          <Text style={styles.footerText}>Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Cart', { cartItems })}>
          <Icon name="shopping-cart" size={30} color="#3498db" />
          <Text style={styles.footerText}>Cart</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#2E294E", 
  },
  header: {
    padding: 20,
    backgroundColor: "#6A0572", 
    alignItems: 'center',
  },
  headerText: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#FFD100", 
  },
  body: {
    flex: 1,
    padding: 20,
  },
  sectionTitle: {
    fontSize: 30,
    fontWeight: "bold",
    marginBottom: 30,
    color: "#00A6A6", 
  },
  productList: {
    flexDirection: 'row',
    marginTop: 10,
  },
  productCard: {
    backgroundColor: "#805C87", 
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginRight: 10,
    width: 150,
  },
  productImage: {
    width: 150,
    height: 150,
    borderRadius: 10,
    resizeMode: 'cover',
  },
  productName: {
    fontSize: 16,
    fontWeight: "bold",
    paddingVertical: 8,
    color: "#FFD100", 
    textAlign: 'center',
  },
  productPrice: {
    fontSize: 14,
    color: "#FFFFFF", 
    paddingBottom: 8,
    textAlign: 'center',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    paddingVertical: 10,
    backgroundColor: "#6A0572", 
  },
  footerText: {
    textAlign: 'center',
    color: '#00A6A6', 
    marginTop: 5,
  },
  productFooter: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default HomeScreen;