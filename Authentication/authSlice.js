import { createSlice } from '@reduxjs/toolkit'; //test

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    user: {
      username: '',
      email: '',
      password: '',
      confirmPassword: '',
    },
    errors: {},
  },
  reducers: {
    storeUser: (state, action) => {
      state.user = action.payload;
    },
    validateForm: (state) => {
      let errors = {};

      if (!state.user.username) {
        errors.username = 'Please fill this field';
      }

      if (!state.user.email) {
        errors.email = 'Please enter a valid email';
      }

      if (!state.user.password) {
        errors.password = 'Please fill this field';
      }

      if (!state.user.confirmPassword) {
        errors.confirmPassword = 'Please fill this field';
      }

      state.errors = errors;
    },
  },
});

export const { storeUser, validateForm } = authSlice.actions;

export default authSlice.reducer;
