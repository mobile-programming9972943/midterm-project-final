import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, ImageBackground } from 'react-native';
import ThemeButton from '../components/ThemeButton';

const backgroundImage = require('../assets/images/bg1.png'); //test

export default function ForgotPassword({ navigation }) {
    const [email, setEmail] = useState('');
    const [resetStatus, setResetStatus] = useState(null);

    const handleResetPassword = () => {
        // placeholder code that simulates a successful request.

        // Simulate a successful password reset request
        setTimeout(() => {
            setResetStatus('Password reset email sent successfully.');
        }, 1000); // Simulated 2-second delay

    };

    return (
        <ImageBackground source={backgroundImage} style={styles.backgroundImage}>
            <View style={styles.container}>
                <Text style={styles.title}></Text>

        <View style={styles.container}>
            <Text style={styles.title}>Forgot Password</Text>
            <TextInput
                style={styles.input}
                placeholder="Email"
                value={email}
                onChangeText={(value) => setEmail(value)}
            />
            <ThemeButton
                title="Send Reset Link"
                onPress={handleResetPassword} // Call the password reset function
            />
            {resetStatus && <Text style={styles.resetStatus}>{resetStatus}</Text>}
        </View>
        </View>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    backgroundImage: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
    },
    title: {
        fontSize: 24,
        marginBottom: 20,
    },
    input: {
        width: 300,
        height: 40,
        borderColor: 'gray',
        backgroundColor: '#F2F2F2',
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: 10,
        marginBottom: 10,
    },
    resetStatus: {
        marginTop: 20,
        fontSize: 13,
        color: 'green',
    },
});
